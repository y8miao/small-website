import pandas as pd
import numpy as np
import spacy
from lexical_diversity import lex_div as ld

from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

nlp = spacy.load("en_core_web_sm")
analyser = SentimentIntensityAnalyzer()

# wordlist data
self_list = ['i', 'me', 'my', 'myself', 'mine', 'we', 'us', 'our', 'ourselves', 'customer', 'customers',
             'shopper', 'shoppers', 'client', 'clients']
neg_list = ['abysmal', 'adverseal', 'arming', 'angry', 'annoy', 'anxious', 'apathy', 'appalling', 'atrocious', 'awful',
             'bad', 'banal', 'barbed', 'belligerent', 'bemoan', 'beneath', 'boring', 'broken',
             'callous', 'cant', 'clumsy', 'coarse', 'cold', 'cold-hearted', 'collapse', 'confused',
             'contradictory', 'contrary', 'corrosive', 'corrupt', 'crazy', 'creepy','criminal','cruel', 'crycutting',
             'damage', 'damaging', 'dastardly','dead', 'decaying','deformed', 'deny', 'deplorable' 'depressed', 'deprived',
             'despicable', 'detrimental', 'dirty', 'disease', 'disgusting', 'disheveled', 'dishonest', 'dishonorable',
             'dismal', 'distress', 'dont', 'dreadful', 'dreary',
             'enraged', 'eroding', 'evil',
             'fail', 'faulty', 'fear', 'feeble', 'fight', 'filthy', 'foul', 'frighten', 'frightful',
             'gawky', 'ghastly', 'grave', 'greed', 'grim', 'grimace', 'gross', 'grotesque', 'gruesome', 'guilty',
             'haggard', 'hard', 'hard-hearted', 'harmful', 'hate', 'hideous', 'homely' 'horrendous',
             'horrible', 'hostile', 'hurthurtful',
             'icky', 'ignorant', 'ignore', 'ill', 'immature', 'imperfect', 'impossible', 'inane', 'inelegant', 'infernal',
             'injure', 'injurious', 'insane', 'insidious', 'insipid',
             'jealous', 'junky',
             'lose', 'lousy', 'lumpy',
             'malicious', 'mean', 'menacing', 'messy', 'misshapen', 'missing', 'misunderstood', 'moan', 'moldy', 'monstrous',
             'naive', 'nasty', 'naughty', 'negate', 'negative', 'never', 'nonobody', 'nondescript', 'nonsense', 'not', 'noxious',
             'objectionable', 'odious', 'offensive', 'oldoppressive',
             'pain', 'perturb', 'pessimistic', 'petty', 'plain', 'poisonous', 'poor', 'prejudice',
             'questionable', 'quirky', 'quit',
             'reject', 'renege', 'repellant', 'reptilian', 'repugnant', 'repulsive', 'revenge', 'revolting', 'rocky',
             'rotten', 'rude', 'ruthless',
             'sad', 'savage' , 'scare', 'scary', 'scream', 'severe', 'shocking', 'shoddy', 'sick', 'sickening', 'sinister',
             'slimy', 'smelly', 'sobbing', 'sorry', 'spiteful', 'sticky', 'stinky', 'stormy', 'stressful', 'stuck',
             'stupid', 'substandard', 'suspect', 'suspicious',
             'tense', 'terrible', 'terrifying', 'threatening',
             'ugly', 'undermine', 'unfair', 'unfavorable', 'unhappy', 'unhealthy', 'unjust', 'unlucky', 'unpleasant',
             'unsatisfactory', 'unsightly', 'untoward', 'unwanted', 'unwelcome', 'unwholesome', 'unwieldy', 'unwise', 'upset',
             'vice', 'vicious', 'vile', 'villainous', 'vindictive',
             'wary', 'weary', 'wicked', 'woeful', 'worthless', 'wound',
             'yell', 'yucky',
             'zero']
emo_list = [        'acceptance', 'admiration', 'adoration', 'affection', 'afraid', 'aggravation', 'aggressive', 'agitation',
                    'agony', 'agreeable', 'alarm', 'alienation', 'amazement', 'amusement', 'anger', 'angry', 'anguish', 'annoyance',
                    'anticipation', 'anxiety', 'apprehension', 'assertive', 'assured', 'astonishment', 'attachment',
                    'attraction', 'awe',
                    'beleaguered', 'bewitched', 'bitterness', 'bliss', 'blue', 'boredom',
                    'calculating', 'calm', 'capricious', 'caring', 'cautious', 'charmed', 'cheerful', 'closeness', 'compassion',
                    'complacent', 'compliant', 'composed', 'conceited', 'concerned', 'contempt', 'content', 'contentment',
                    'crabby', 'crazed', 'crazy', 'crosscruel',
                    'defeated', 'defiance', 'delighted', 'dependence', 'depressed', 'desire', 'disappointment', 'disapproval',
                    'discontent', 'disenchanted', 'disgust', 'disillusioned', 'dislike', 'dismay', 'displeasure', 'dissatisfied',
                    'distraction', 'distress', 'disturbed', 'dread',
                    'eager', 'earnest', 'easygoing', 'ecstasy', 'ecstatic', 'elation', 'embarrassment', 'emotion', 'emotional',
                    'enamored', 'enchanted', 'enjoyment', 'enraged', 'enraptured', 'enthralled', 'enthusiasm', 'envious', 'envy',
                    'equanimity', 'euphoria', 'exasperation', 'excited', 'exhausted', 'extrovert', 'edexuberant',
                    'fascinated', 'fatalistic', 'fear', 'fearful', 'ferocity', 'flummoxed', 'flustered', 'fondness', 'fright',
                    'frightened', 'frustration', 'furious', 'fury',
                    'generous', 'glad', 'gloating', 'gloomy', 'glum', 'greedy', 'grief', 'grim', 'grouchy', 'grumpy', 'guilt',
                    'happiness', 'happy', 'harried', 'homesick', 'hopeless', 'horror', 'hostility', 'humiliation', 'hurt', 'hysteria',
                    'infatuated', 'insecurity', 'insulted', 'interested', 'introverted', 'irritation', 'isolation',
                    'jaded', 'jealous', 'jittery', 'jolliness', 'jolly', 'joviality', 'joy', 'jubilation',
                    'keen', 'kind', 'kindhearted', 'kindly',
                    'laidback', 'lazy', 'like', 'liking', 'loathing', 'loneliness', 'lonely', 'longing', 'love', 'lulled', 'lust',
                    'mad', 'merry', 'misery', 'modesty', 'mortification',
                    'naughty', 'neediness', 'neglected', 'nervous', 'nirvana',
                    'open', 'optimism', 'ornery', 'outgoing', 'outrage',
                    'panic', 'passion', 'passive', 'peaceful', 'pensive', 'pessimism', 'pity', 'placid', 'pleased', 'pride',
                    'proud', 'pushy',
                    'quarrelsome', 'queasy', 'querulous', 'quickwitted', 'quiet', 'quirky',
                    'rage', 'rapture', 'rejection', 'relief', 'relieved', 'remorse', 'repentance', 'resentment', 'resigned',
                    'revulsion', 'roused',
                    'sad', 'sadness', 'sarcastic', 'sardonic', 'satisfaction', 'scared', 'scorn', 'selfassured', 'selfcongratulatory',
                    'selfsatisfied', 'sentimentality', 'serenity', 'shame', 'shock', 'smug', 'sorrow', 'sorry', 'spellbound',
                    'spite', 'stingy', 'stoical', 'stressed', 'subdued', 'submission', 'suffering', 'surprise', 'sympathy',
                    'tenderness', 'tense', 'terror', 'threatening', 'thrill', 'timidity', 'torment', 'tranquil',
                    'triumphant', 'trust',
                    'uncomfortable', 'unhappiness', 'unhappy', 'upset',
                    'vain', 'vanity', 'venal', 'vengeful', 'vexed', 'vigilance', 'vivacious',
                    'wary', 'watchfulness', 'weariness', 'weary', 'woe', 'wonder', 'worried', 'wrathful',
                    'zeal', 'zest']

# read in company data
df_author = pd.read_csv('C:\\Users\\Youchen Miao\\Downloads\\author_analysis-author_new_output.csv',low_memory=False,encoding='cp1252')


df_sentences = df_author[['ReviewerURL','authorBusinessName','authorReviewText','authorLike']]


descriptive = []
action = []
like = []
emotional = []
self_ref = []
negation = []
sentiment_high = []
sentiment_low = []
lexical_diversity = []

for index, row in df_sentences.iterrows():
    # finding number descriptive and action words for each review
    if pd.isnull(row['authorReviewText']):
        descriptive.append(0)
        action.append(0)
        sentiment_high.append(0)
        emotional.append(0)
        self_ref.append(0)
        sentiment_low.append(0)
        negation.append(0)
        lexical_diversity.append(0)
    else:
        doc = nlp(row['authorReviewText'])
        flt = ld.flemmatize(row['authorReviewText'])
        lexical_score = ld.ttr(flt)
        lexical_diversity.append(lexical_score)
        verb = 0
        adj = 0
        emotion_wc = 0
        self_wc = 0
        neg_wc = 0
        wc = 0
        for word in doc:
            if word.pos_ == 'VERB':
                verb += 1
            elif word.pos_ == 'ADJ':
                adj += 1
            if word.lemma_ in emo_list:
                emotion_wc += 1
            if word.lemma_ in self_list:
                self_wc += 1
            if word.lemma_ in neg_list:
                neg_wc += 1
            wc += 1
        emotional.append(emotion_wc/wc)
        descriptive.append(adj/wc)
        self_ref.append(self_wc/wc)
        negation.append(neg_wc/wc)
        action.append(verb/wc)
        sentiment_score = analyser.polarity_scores(row['authorReviewText'])
        if sentiment_score.get('compound') >= 0.7:
            sentiment_high.append(1)
            sentiment_low.append(0)
        elif sentiment_score.get('compound') <= -0.7:
            sentiment_high.append(0)
            sentiment_low.append(1)
        else:
            sentiment_high.append(0)
            sentiment_low.append(0)
    # finding whether there is like or not
    like_string = row['authorLike']
    if like_string == 'Like':
        like.append(0)
    else:
        like.append(1)
    print(len(like))


df_sentences['descriptive_words'] = np.array(descriptive)
df_sentences['action_words'] = np.array(action)
df_sentences['liked'] = np.array(like)
df_sentences['self_reference'] = np.array(self_ref)
df_sentences['emotional_words'] = np.array(emotional)
df_sentences['negation_words'] = np.array(negation)
df_sentences['sentiment_high'] = np.array(sentiment_high)
df_sentences['sentiment_low'] = np.array(sentiment_low)
df_sentences['lexical_diversity'] = np.array(lexical_diversity)


df_sentences = df_sentences[['ReviewerURL','authorBusinessName','descriptive_words','action_words','liked','self_reference','emotional_words','negation_words','sentiment_high','sentiment_low','lexical_diversity']]
df_result = pd.merge(df_author,df_sentences, on=['ReviewerURL','authorBusinessName'], how='left')
df_result = df_result.drop_duplicates().fillna(0)
df_result.to_csv('C:\\Users\\Youchen Miao\\Downloads\\Author_data_new_features.csv',index=False)
print(df_result)