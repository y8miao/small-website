class Category:
    def __init__(self,name):
        """
        Constructor initializes score and num_data dictionary as empty dictionaries

        :param string name: Name of the evaluation table
        """
        self.score = 0.0
        self.num_data = 0
        self.word_list = {}
        self.word_sentiment = {}
        self.sorted_contribute_list = {}
        self.sentence_score = {}
        self.sorted_feature_list = {}
        self.name = name

    def updateData(self, sentence, word, score):
        """
        Function that inserts matched sentence,word(noun,adjective based on category or subcategory),score

        :param sentence:
        :param word:
        :param score:
        :return:
        """
        # update the sentence_score dictionary, update score for sentence if exist, insert if not already exist
        if sentence in self.sentence_score:
            old = self.sentence_score[sentence]
            if abs(old)<abs(score):
                self.sentence_score[sentence] = score
        else:
            sentence[sentence] = score
        # update word_list and word_sentiment
        if word in self.word_list and word in self.word_sentiment:
            temp = self.word_sentiment[word]
            self.word_sentiment[word] = (temp*self.word_list[word]+score)/(self.word_list[word]+1)
            self.word_list[word] += 1
        elif not(word in self.word_list) and not(word in self.word_sentiment):
            self.word_list[word] = 1
            self.word_sentiment[word] = score
        else:
            print("error in updateData: data exists in one of word_list/word_sentiment but not both.")
            return -1
        # update score and num_data
        self.score += score
        self.num_data += 1
        return 0

    def sortWordFrequency(self):
        """
        sort the dictionary word_list and store in sorted_contribute_list

        :return:
        """
        dictionary = self.word_list
        sorted_list = sorted(dictionary.items(),reverse=True,key=lambda x:x[1])
        self.sorted_contribute_list = sorted_list


    def sortWordSentiment(self):
        """
        sort the dictionary word_sentiment and store in sorted_feature_list

        :return:
        """
        dictionary = self.word_sentiment
        sorted_list = sorted(dictionary.items(),reverse=True,key=lambda x:x[1])
        self.sorted_feature_list = sorted_list


    def topContribute(self,num):
        """


        :param num:
        :return:
        """
        sorted_list = self.sorted_contribute_list
        if num <= 0:
            return []
        if num >= len(sorted_list):
            return sorted_list
        else:
            return sorted_list[0:num - 1]

    def topFeature(self,num):
        """

        :param num:
        :return:
        """
        sorted_list = self.sorted_feature_list
        if num <= 0:
            return []
        if num >= len(sorted_list):
            return sorted_list
        else:
            return sorted_list[0:num - 1]

    def topSentence(self,num):
        """

        :param num:
        :return:
        """
        dictionary = self.word_list
        sorted_list = sorted(dictionary.items(), reverse=True, key=lambda x: x[1])
        if num <= 0:
            return []
        if num >= len(sorted_list):
            return sorted_list
        else:
            return sorted_list[0:num - 1]

    def getScore(self):
        """

        :return:
        """
        return self.score/self.num_data

    def getNumData(self):
        """

        :return:
        """
        return self.num_data

    def getName(self):
        """

        :return:
        """
        return self.name


class  Subcategory(Category):
    def __init__(self,name):
        Category.__init__(self,name)
        self.list_category = []
        self.num_attached = 0

    def attachCategory(self,category):
        """

        :param category:
        :return:
        """
        self.list_category.append(category)
        self.num_attached += 1

    def dettachCategory(self,name):
        """

        :param name:
        :return:
        """
        for category in self.list_category:
            if category.name == name:
                self.list_category.remove(category)
                return 0
        print("cannot find category {}.".format(name))
        return -1