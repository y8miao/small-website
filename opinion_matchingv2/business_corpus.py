import category
import re
import os
import pandas as pd


def getPair(pair):
    """
    Helper function for opinionMatching that separates the string pair into list pair of words

    :param string pair: string with format ['first word','second word']
    :return: list element containing 2 words in order
    """
    result = re.sub("[^\w|-]", " ", pair).split()
    return result


class BusinessCorpus:
    def __init__(self,name):
        self.business_type = name
        self.all_adjectives = {}
        self.all_nouns = {}
        self.subcategory_dict = {}
        self.category_dict = {}

    def corpusInput(self,file):
        """

        :param file:
        :return:
        """
        #special file process
        all_adj_file = "All_Adjectives"
        all_noun_file = "All_Nouns"
        temp_df = pd.read_csv(os.path.join(file,all_adj_file+".csv"))
        for index,row in temp_df.iterrows():
            self.all_adjectives.update(re.sub("[^\w]", " ", row['Terms']).split())
        temp_df = pd.read_csv(os.path.join(file, all_noun_file + ".csv"))
        for index, row in temp_df.iterrows():
            self.all_adjectives.update(re.sub("[^\w]", " ", row['Terms']).split())
        files = []
        f = open(os.path.join(file, "categories.txt"), "r", encoding="utf-8")
        for line in f:
            # each category is a tuple of nouns representing the category and a dictionary of subcategories
            files.append(line)
        # import subcategories from each individual category group
        for filename in files:
            catname = ''
            for character in filename:
                if character == '_':
                    break
                else:
                    catname+=character
            # if category doesn't already exist, add it to corpus_dict
            if not(self.categoryExist(catname)):
                #read in corpus file
                cat_df = pd.read_csv(os.path.join(file,filename+".csv"))
                #create noun and adjective sets for addCategory
                noun_set = {}
                adj_set = {}
                for index,row in cat_df.iterrows():
                    noun_set.update(re.sub("[^\w]", " ", row['Nouns']).split())
                    adj_set.update(re.sub("[^\w]", " ", row['Adjectives']).split())
                cat = category.Category(catname)
                #filename is also the subcategory name
                subcat = category.Subcategory(filename)
                subcat.attachCategory(cat)
                self.addCategory(cat, catname)
                self.addSubcategory(subcat,filename,noun_set,adj_set)
            # if category already exists, add subcategory to category
            else:
                cat_df = pd.read_csv(os.path.join(file, filename + ".csv"))
                # create noun and adjective sets for addCategory
                noun_set = {}
                adj_set = {}
                for index, row in cat_df.iterrows():
                    noun_set.update(re.sub("[^\w]", " ", row['Nouns']).split())
                    adj_set.update(re.sub("[^\w]", " ", row['Adjectives']).split())
                cat = self.category_dict[catname]
                # filename is also the subcategory name
                subcat = category.Subcategory(filename)
                subcat.attachCategory(cat)
                self.addSubcategory(subcat, filename, noun_set, adj_set)

    def corpusOutput(self,file):
        """

        :param file:
        :return:
        """
        """
            Function that outputs corpus from corpus dictionary to a given directory

            :param string directory_path:
            :param dictionary corpus_dict:
            :return: n/a
        """
        cat_list = []
        ter_list = []
        for key,list in self.subcategory_dict:
            # insert terms into cat_dict, list[0] has object subcategory, list[1] has noun_set, list[2] has adj_set
            cat_list.append(list[0].getName())
            noun_terms = ','.join(str(element) for element in list[1])
            adj_terms = ','.join(str(element) for element in list[2])
            # if noun_terms is empty, put a stirng with space as place holder
            if not noun_terms:
                ter_list.append(" ")
            else:
                ter_list.append(noun_terms)
            outfile = list[0].getName() + '.csv'
            output_df = pd.DataFrame()
            output_df['Nouns'] = noun_terms
            output_df['Adjectives'] = adj_terms
            output_df.to_csv(os.path.join(file, outfile), index=False)


    def addCategory(self,category, name):
        """

        :param name: String containing name of the category
        :return:
        """
        self.category_dict[name] = category

    def addSubcategory(self,subcategory, name, noun_set, adj_set):
        """

        :param subcategory:
        :param noun_set:
        :param adj_set:
        :return:
        """
        subcategory_profile = [subcategory,noun_set,adj_set]
        self.subcategory_dict[name] = subcategory_profile

    def removeCategory(self,name):
        """

        :param name:
        :return:
        """
        pass


    def removeSubcategory(self,name):
        """

        :param name:
        :return:
        """
        pass


    def opinionMatching(self, infile):
        """"""
        df = pd.read_csv(infile)
        unmatched_noun = []
        unmatched_adj = []
        matched_noun = []
        matched_adj = []
        matched_score = []
        column = []
        # match indicater for whether the pair should be reported for unmatched.
        matched = 0
        lop = df['lemmatization'].values.tolist()
        los = df['score'].values.tolist()
        losentence = df['sentence'].values.tolist()
        for i in range(len(lop)):
            #unmatched to start with
            matched = 0
            # acquire the pair from string value
            pair_tuple = getPair(lop[i])
            # issue in extraction that sometimes extracts things with only a symbol like ('t','''), prevents index error in the following lines.
            if len(pair_tuple) < 2:
                column.append('bad input')
                continue
            noun = pair_tuple[0]
            adj = pair_tuple[1]
            score = los[i]
            # start matching the noun and adjective for subcategory by going through all subcategories
            #when noun and adjectives are both in all, exp: store is good/bad, every field is incremented
            if noun in self.all_nouns and adj in self.all_adjectives:
                matched = 1
                for key, value in self.subcategory_dict:
                    subcategory = value[0]
                    # update sentence, word_pair and score
                    subcategory.updateData(losentence[i],adj,los[i])
                    for category in subcategory.list_category:
                        category.updateData(losentence[i],noun, los[i])
            #when noun is generic, check adjectives
            elif noun in self.all_nouns:
                for key, value in self.subcategory_dict:
                    subcategory = value[0]
                    adj_set = value[2]
                    if adj in adj_set:
                        matched = 1
                        # update sentence, word_pair and score
                        subcategory.updateData(losentence[i], adj, los[i])
                        for category in subcategory.list_category:
                            category.updateData(losentence[i], noun, los[i])
            elif adj in self.all_adjectives:
                for key, value in self.subcategory_dict:
                    subcategory = value[0]
                    noun_set = value[1]
                    if noun in noun_set:
                        matched = 1
                        # update sentence, word_pair and score
                        subcategory.updateData(losentence[i], adj, los[i])
                        for category in subcategory.list_category:
                            category.updateData(losentence[i], noun, los[i])
            else:
                for key,value in self.subcategory_dict:
                    subcategory = value[0]
                    noun_set = value[1]
                    adj_set = value[2]
                    if noun in noun_set and adj in adj_set:
                        matched = 1
                        # update sentence, word_pair and score
                        subcategory.updateData(losentence[i],(noun,adj),los[i])
                        # in most scenarios, list_category will only have 1 category, check note.txt
                        for category in subcategory.list_category:
                            category.updateData(losentence[i],(noun,adj),los[i])
            if matched == 0:
                unmatched_noun.append(noun)
                unmatched_adj.append(adj)
            else:
                matched_noun.append(noun)
                matched_adj.append(adj)
                matched_score.append(score)
        out_df = pd.DataFrame()
        out_df['nouns'] = unmatched_noun
        out_df['adjectives'] = unmatched_adj
        out_df.to_csv("unmatched.csv")
        word_cloud_df = pd.DataFrame()
        word_cloud_df['nouns'] = matched_noun
        word_cloud_df['adjectives'] = matched_adj
        word_cloud_df['scores'] = matched_score